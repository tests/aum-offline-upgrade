#!/bin/sh

if [ $(id -u) -ne 0 ]; then
	echo "$0 should be run as root"
	exit 1
fi

SCRIPTDIR=$(realpath $(dirname "$0"))
sed "s#@path@#${SCRIPTDIR}#" ${SCRIPTDIR}/boot-delay.service.in > /tmp/boot-delay.service

if [ "$2" = "--to-console" ]; then
	sed -i "s#/dev/tty1#/dev/console#" /tmp/boot-delay.service
fi

mv /tmp/boot-delay.service /etc/systemd/system/

systemctl daemon-reload
systemctl enable boot-delay.service
