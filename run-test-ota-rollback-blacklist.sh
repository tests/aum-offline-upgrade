#!/bin/sh
set -ex

. ./update-manager-common.sh

phase_boot()
{
	# Prepare the skiplist
	# Rationale: https://phabricator.apertis.org/T5881
	# In additon: remove updatectl to prevent from marking update as successful
	ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo /usr/bin/updatectl > /tmp/skip

	ostree admin status

	# Create a new commit based on current deployment, but older and without files from a skip list
	BRANCHNAME=$(branch_name)
	CURREV=$(ostree rev-parse origin:$BRANCHNAME)
	ostree commit -b $BRANCHNAME --tree=ref=$CURREV --timestamp="1 year ago" --skip-list=/tmp/skip
	OLDREV=$(ostree rev-parse $BRANCHNAME)
	ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$OLDREV

	# Emulate the ugrade state -- set  bootcounter to 0 and the upgrade flag
	set_bootcounter 0 1

	# Store the commit with prepared revision for use in tests later
	echo $OLDREV > $(get_phase_data_path)
	sync
}

phase_check_update()
{
	ostree admin status

	# Enable debug for AUM to check how it deal with upgrade&rollback
	enable_aum_debug
	journalctl --no-pager -u apertis-update-manager

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_EXPECTED=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_EXPECTED" != "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same as the initial commit"
		exit 1
	fi

	# Check the bootcounter state
	# It may have different boot count on different boards due LAVA
	check_bootcounter "" "" "" 01

	# Emulate the ugrade state -- set the upgrade flag and bootcounter to any value above the limit
	# This should to trigger the U-Boot to boot previous version after reboot
	set_bootcounter 5
}

phase_check_rollback()
{
	# Wait long enough so that AUM have a chance to finish undeploy
	sleep 30

	ostree admin status
	journalctl --no-pager -u apertis-update-manager

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not rollback, the current commit is the same"
		exit 1
	fi

	# after the rollback, the apertis update manager resets the bootcount
	# Ensure to have non-upgrade state
	check_bootcounter "" "" 00 00

	# Must exists and contain "failed" upgrade
	cat /var/aum_blacklist.conf

	# Prepare outdated commit from the booted one
	BRANCHNAME=$(branch_name)
	ostree commit -b $BRANCHNAME --tree=ref=$COMMIT_AFTER --timestamp="1 year ago"
	OLDREV=$(ostree rev-parse $BRANCHNAME)
	ostree admin upgrade --os=apertis --allow-downgrade --deploy-only --override-commit=$OLDREV
}

phase_clean_previous_install()
{
	clean_previous_install

	# Add the newest commit on server side into blacklist to prevent upgrading
	# Usually it should be the same as installed
	BRANCHNAME=$(branch_name)
	ostree pull --commit-metadata-only --depth=1 origin "$BRANCHNAME"
	BLACKLISTREV=$(ostree rev-parse origin:$BRANCHNAME)
	echo "$BLACKLISTREV=true" >> /var/aum_blacklist.conf
	sync
}

phase_check_blacklist()
{
	ostree admin status
	cat /var/aum_blacklist.conf

	# OTA update is blacklisted
	if apply_update_sync -o ; then
		exit 1
	fi
}


update_manager_phases\
	phase_boot\
	phase_check_update\
	phase_check_rollback\
	phase_clean_previous_install\
	phase_check_blacklist
