#!/bin/sh

. ./update-manager-common.sh

phase_boot()
{
	prepare_outdated_commit
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_update()
{
	ostree admin status

	# Enable automatic OTA updates in configuration file
	sed -i -e "s/^UpdatesFromNetwork=.*/UpdatesFromNetwork=true/" /etc/apertis-update-manager.ini

	apply_update_sync -c

	check_current_status "$STATUS_PENDING"
}

phase_check_update()
{
	ostree admin status

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same"
		exit 1
	fi
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_update\
	phase_check_update
