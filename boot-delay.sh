#!/bin/sh

EFIBOOTDIR="/boot/efi/loader/entries"
LOOP=30

if [ -f /boot/uboot.cnt ]; then
	uboot_counter=$(od -t x1 -An /boot/uboot.cnt)
	m=$(echo $uboot_counter | cut -d ' ' -f 1)
	if [ "$m" = "bc" ]; then
		cnt=$(echo $uboot_counter | cut -d ' ' -f 2)
		# NB: due the bug in legacy U-Boot -- it always create the uboot.cnt and
		# increase the counter, so it is not possible to detect the upgrade
		# state properly. Hence set the upgrade state for the legacy.
		uflag="01"
	else
		cnt=$(echo $uboot_counter | cut -d ' ' -f 3)
		uflag=$(echo $uboot_counter | cut -d ' ' -f 4)
	fi
elif [ -f ${EFIBOOTDIR}/ostree-0-2*.conf ]; then
	suffix=$(ls ${EFIBOOTDIR}/ostree-0-2*.conf | cut -d "+" -f 2 -s | cut -d "." -f 1 -s)
	if [ -n "$suffix" ]; then
		uflag="01"
		cnt=$(echo $suffix | cut -d "-" -f 2 -s)
		if [ -z "$cnt" ]; then
			cnt="00"
		else
			cnt=$(printf "%02d" "$cnt")
		fi
	fi
fi

if [ "$uflag" != "01" ]; then
	exit 0
else
	echo "Boot count: $cnt"
fi

while [ $LOOP -gt 0 ]; do
	echo "Resuming boot in $((LOOP=LOOP - 1)) seconds"
	sleep 1
done
echo "Resuming boot"
