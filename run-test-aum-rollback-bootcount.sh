#!/bin/sh

source /etc/os-release

ARCH=`dpkg --print-architecture`
BASEURL="https://images.apertis.org" BOARD="uboot" IMGPATH="daily/$VERSION"

sudo common/run-test-in-systemd --timeout=1200 --name=rollback-bootcount env DEBUG=2 RELEASE=$VERSION ARCH=$ARCH BASEURL=$BASEURL IMGPATH=$IMGPATH IMGDATE=$BUILD_ID IMGTYPE=$VARIANT_ID BOARD=$BOARD ./run-test-ota-auto.sh
