#!/bin/sh

. ./update-manager-common.sh

set -u

# Here we expect the system booted with the image from the previous release
phase_boot_update ()
{
	# Enable debug output from AUM
	enable_aum_debug

	ostree admin status
	# ostree admin status
	# * apertis bab35cb1fe04e2fc11c3b462ebac0e38640173ebcff55ca326f838296a0121f5.0
	#     origin refspec: origin:apertis/v2020/armhf-uboot/minimal

	ostree refs
	ostree refs -c

	TARGET_BRANCH=$(branch_name)
	echo TARGET_BRANCH="$TARGET_BRANCH"

	# Need a branch from the refspec, i.e. "apertis/v2020/armhf-uboot/minimal"
	echo "Get the booted origin, which should be from a version preceding the one being tested"
	BOOT_BRANCH=$(ostree admin status | grep "origin refspec:" | head -n 1 | sed -e s/".*origin:"//)
	echo BOOT_BRANCH="$BOOT_BRANCH"

	if [ "$TARGET_BRANCH" = "$BOOT_BRANCH" ]; then
		echo "The current branch ($BOOT_BRANCH) is the same as the target one"
		exit 1
	fi

	# Cut the "version" field from "apertis/v2020/armhf-uboot/minimal"
	# result: "v2020"
	BOOT_RELEASE=$(echo $BOOT_BRANCH | cut -d '/' -f 2)

	echo "Expecting update from $BOOT_RELEASE to $RELEASE"

	#Special case for switching from v2020 -> v2021
	# v2020 did not support delta files with signed metadata
	DELTA_SUFFIX=""
	# Just remove suffix from the version, to be able to use development Apertis versions
	# as instance, "v2021pre" -> "v2021"
	BOOT_RELEASE="$(echo $BOOT_RELEASE | cut -c1-5)"
	if [ "${BOOT_RELEASE}" = "v2020" ]; then
		DELTA_SUFFIX=".compat-${BOOT_RELEASE}"
	fi

	DELTAFILE=$(get_static_delta "$DELTA_SUFFIX")

	apply_update_sync -d "$DELTAFILE"

	ostree admin status
	ostree refs
	ostree refs -c

	# Store the current revision for use in test later
	CURREV=$(ostree rev-parse origin:"$BOOT_BRANCH")
	echo "$CURREV" > $(get_phase_data_path)
}

phase_check_update()
{
	local RESULT=0
	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat "$PHASE_DATA")

	ostree admin status
	## ostree admin status
	# * apertis 334f57354221f0800b008d043c5c50683e83e8b6afa5880e95449a980132fa83.0
	#     origin refspec: origin:apertis/v2021pre/armhf-uboot/minimal
	#   apertis bab35cb1fe04e2fc11c3b462ebac0e38640173ebcff55ca326f838296a0121f5.0 (rollback)
	#     origin refspec: origin:apertis/v2020/armhf-uboot/minimal

	ostree refs
	ostree refs -c

	# The current commit after the update
	# i.e. parsing "* apertis 334f57354221f0800b008d043c5c50683e83e8b6afa5880e95449a980132fa83.0" into
	# "334f57354221f0800b008d043c5c50683e83e8b6afa5880e95449a980132fa83"
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current and original commit ($COMMIT_BEFORE) match"
		exit 1
	fi

	TARGET_BRANCH=$(branch_name)
	echo TARGET_BRANCH="$TARGET_BRANCH"

	echo "Get the booted origin, which should be from a version under the test"
	# Need a branch from the booted refspec, i.e. "apertis/v2021pre/armhf-uboot/minimal"
	BOOT_BRANCH=$(ostree admin status | grep "origin refspec:" | head -n 1 | sed -e s/".*origin:"//)
	echo BOOT_BRANCH="$BOOT_BRANCH"

	if [ "$TARGET_BRANCH" != "$BOOT_BRANCH" ]; then
		echo "The current branch ($BOOT_BRANCH) is not the same as the target one ($TARGET_BRANCH)"
		RESULT=1
	fi

	echo "Get the origin for previous boot, which should be from a version preceding the one being tested"
	# Need a branch from the rollback refspec, i.e. "apertis/v2020/armhf-uboot/minimal"
	OLD_BRANCH=$(ostree admin status | grep "origin refspec:" | tail -n 1 | sed -e s/".*origin:"//)
	echo OLD_BRANCH="$OLD_BRANCH"

	if [ "$BOOT_BRANCH" = "$OLD_BRANCH" ]; then
		echo "The booted branch ($BOOT_BRANCH) is not updated"
		RESULT=1
	fi

	# Check if all needed refs are on the place
	for ref in "$TARGET_BRANCH" "origin:$TARGET_BRANCH" "-c org.apertis.os"; do
		local REF="$(ostree refs $ref | grep $TARGET_BRANCH)"
		if [ -z "$REF" ]; then
			echo "Absent ref for \"$ref\""
			RESULT=1
		fi
	done

	# Check if no stalled refs after upgrade
	for ref in "$OLD_BRANCH" "origin:$OLD_BRANCH" "-c org.apertis.os"; do
		local REF="$(ostree refs $ref | grep $OLD_BRANCH)"
		if [ -n "$REF" ]; then
			echo "'ostree ref $ref' returned '$REF', it still points to the old branch '$OLD_BRANCH'"
			RESULT=1
		fi
	done

	if [ $RESULT -ne 0 ]; then
		echo "Test failed"
		exit 1
	fi
}


update_manager_phases\
	phase_boot_update\
	phase_check_update
