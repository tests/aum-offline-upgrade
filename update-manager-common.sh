# Common definitions used in tests

#  interface org.apertis.ApertisUpdateManager {
#    methods:
#      RegisterSystemUpgradeHandler(); ok
#      ApplySystemUpgrade();
#      MarkUpdateSuccessful();
#      ApplyStaticDelta(in  s filename); ok
#    signals:
#    properties:
#      readonly b NetworkConnected = true;
#      writeonly b UpdatesFromNetwork;
#      readonly u SystemUpgradeState = 0;
#  };
# $ gdbus call -y -d org.apertis.ApertisUpdateManager -o / -
# m org.freedesktop.DBus.Properties.Get org.apertis.ApertisU
# pdateManager SystemUpgradeState
#(<uint32 0>,)
# 0 Unknown, 1 Checking, 2 Downloading, 3 Deploying
# 100 pending
# 200 uptodate

STATUS_UNKNOWN=0
STATUS_CHECKING=1
STATUS_DOWNLOADING=2
STATUS_DEPLOYING=3
STATUS_PENDING=100
STATUS_UPTODATE=200
GETPROP="gdbus call -y -d org.apertis.ApertisUpdateManager -o / -m org.freedesktop.DBus.Properties.Get org.apertis.ApertisUpdateManager"

# List of functions common to all update manager tests
strip_type() {
	echo $* | sed -e "s/.*<.* \(.*\)>.*/\1/"
}

check_current_status()
{
	local STATE="$(${GETPROP} SystemUpgradeState)"
	STATE=$(strip_type ${STATE})

	if [ "$STATE" != "$1" ] ; then
		echo "Invalid DBUS property value (expected $1, got $STATE)"
		exit 1
	fi
}

get_static_delta()
{
	local DELTAURL=""
	local DELTAFILE=""
	local SUFFIX="$1"
	local SUBDIR=""
	local ENCRYPTED=""
	[ $# -gt 1 ] && SUBDIR="$2/"
	[ $# -gt 2 ] && ENCRYPTED="$3"

	#PLAYGROUND="/test/playground/fredo/build-ostree"
	DELTAURL="${BASEURL}/${IMGPATH}/${IMGDATE}"
	DELTAFILE="${IMGNAME}${SUFFIX}.delta${ENCRYPTED}"
	DELTAURL="${DELTAURL}/${ARCH}/${IMGTYPE}/${SUBDIR}${DELTAFILE}"

	#wget --show-progress=off "${DELTAURL}" > /dev/null
	if [ ! -f "${DELTAFILE}" ] ; then
		busybox wget -q "${DELTAURL}" > /dev/null
	fi

	# Function outputs the name of the delta file
	echo "${DELTAFILE}"
}

apply_update_async()
{
	if [ $# -eq 0 ] ; then
		# Enable network updates
		echo "Enable network updates"
		updatectl --check-network-updates
	else
		# Applying a static delta is asynchronous, updatectl immediately returns
		echo "Applying static delta: $1"
		updatectl --apply-static-delta "$1"
	fi
}

# Options:
# -o -- start OTA upgrade
# -d <file> -- use static delta file
# -c -- just check the update started outside of the function
apply_update_sync()
{
	local RESULT=1
	local DELTAFILE=""
	local OTA=""
	local CHECK=""

	local CURSOR="$(journalctl --show-cursor -n 0 -o cat)"
	CURSOR="${CURSOR#-- cursor: }"

	local OPTS="$(getopt -o "cod:" -l "check,ota,delta:" -- "$@")"

	if [ $# -eq 0 ]; then
		echo "No options passed for upgrade"
		return 1
	fi

	while [ $# -gt 0 ]; do
		case $1 in
			-o|--ota) OTA="TRUE"; shift; break;;
			-d|--delta) DELTAFILE="$2"; shift 2; break;;
			-c| --check) CHECK="TRUE"; shift; break;;
			--) shift;;
			*)  echo "Unexpected option: $1"
			return 1;;
		esac
	done

	journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager -ef & JOURNALPID=$!

	systemctl restart apertis-update-manager

	# Flush logs to avoid graving old logs later
	journalctl --flush

	## Start the handler in nohup because LAVA waits for all processes to terminate (like ssh)
	# and if the handler quits, AUM reboots. And if AUM reboots, LAVA timeouts.
	nohup updatectl --register-upgrade-handler & UPGRADEHANDLERPID=$!

	# Update the cursor to catch events after restart
	CURSOR="$(journalctl --show-cursor --unit apertis-update-manager -n 0 -o cat)"
	CURSOR="${CURSOR#-- cursor: }"

	# Applying update is asynchronous, updatectl spawns the update and returns immediately
	if [ -n "$OTA" ]; then
		apply_update_async
	elif [ -n "$DELTAFILE" ]; then
		apply_update_async "$DELTAFILE"
	elif [ -n "$CHECK" ]; then
		echo "Waiting for update"
	else
		echo "Unexpected state"
		return 1
	fi

	for i in $(seq 0 99); do
		sleep 5
		# If update finished
		if journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager | grep -qE "Ostree already up to date" ; then
			echo "no update done"
			RESULT=1
			break
		fi
		if journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager | grep -qE "Ostree upgrade failed" ; then
			echo "update failed"
			RESULT=1
			break
		fi
		if journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager | grep -qE "Ostree upgrade ready, system should be rebooted" ; then
			echo "ok"
			RESULT=0
			break
		fi
		n_restart=`journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager | grep -E "Starting apertis-update-manager.service" | wc -l`
		if [ $n_restart -gt 1 ] ; then
			echo "AUM crashed"
			RESULT=99
			break
		fi
	done

	# Print the logs if a failure has been detected
	if [ "$RESULT" -ne "0" ]; then
		echo "Failure logs:"
		journalctl --after-cursor "${CURSOR}" --unit apertis-update-manager
	fi

	# kill the upgrade handler
	# Requires update-manager 0.1812.1 to avoid reboot when
	# the handler disconnects
	kill $UPGRADEHANDLERPID

	# Terminate journal logging
	kill $JOURNALPID

	# Do not use exit in functions because it prevents lava to terminate
	return $RESULT
}

get_phase_data_path()
{
	echo /var/run-phase-data
}

error_occured()
{
	set +x
	echo "failed $PHASE" > /var/run-phase
	sync
}

update_manager_phases()
{
	set -eux

	RUNPHASE=/var/run-phase
	PHASE=$(cat $RUNPHASE | head -1 || true)


	# The first phase is phase_boot
	case "$PHASE" in
	"") PHASE="0";;
	"done")
		exit 0;;
	failed\ *)
		echo "Canceled phase because an error occured earlier"
		exit 1 ;;
	0|1|2|3|4|5|6|7|8|9)
		;;
	*)
		echo "Wrong phase file $PHASE"
		exit 1 ;;
	esac

	trap error_occured EXIT

	# Move to a writable directory stable accross reboots
	cd /var

	shift $PHASE

	ostree admin status
	echo "-=< Current phase: $1 >=-"
	set -x
	eval $1
	set +x

	# Prepare next phase
	if [ $# -gt 1 ] ; then
		echo $(($PHASE + 1)) > $RUNPHASE
	else
		echo "Test reached last phase: $PHASE"
		echo "done" > $RUNPHASE
	fi
	sync

	trap '' EXIT

	echo "finished"
}

clean_previous_install()
{
	ostree admin status

	check_current_status "$STATUS_UNKNOWN"

	# Remove the origin branch
	ostree admin undeploy 1
	ostree refs --delete master

	ostree admin cleanup
}

# Keys for ed25519 signing tests
ED25519PUBLIC=
ED25519SEED=
ED25519SECRET=

gen_ed25519_keys ()
{
  # Generate private key in PEM format
  pemfile="$(mktemp --tmpdir ed25519_XXXXXX.pem)"
  openssl genpkey -algorithm ed25519 -outform PEM -out "${pemfile}"

  # Based on: http://openssl.6102.n7.nabble.com/ed25519-key-generation-td73907.html
  # Extract the private and public parts from generated key.
  ED25519PUBLIC="$(openssl pkey -outform DER -pubout -in ${pemfile} | tail -c 32 | base64)"
  ED25519SEED="$(openssl pkey -outform DER -in ${pemfile} | tail -c 32 | base64)"
  # Secret key is concantination of SEED and PUBLIC
  ED25519SECRET="$(echo ${ED25519SEED}${ED25519PUBLIC} | base64 -d | base64 -w 0)"
}

gen_ed25519_random_public()
{
  openssl genpkey -algorithm ED25519 | openssl pkey -outform DER | tail -c 32 | base64
}

# Enable debug messages for AUM
enable_aum_debug ()
{
  AUMDIR=/etc/systemd/system/apertis-update-manager.service.d
  mkdir -p "${AUMDIR}"
  tee "${AUMDIR}"/override.conf <<E_O_F
[Service]
Environment=G_DEBUG=G_LOG_LEVEL_DEBUG
Environment=G_MESSAGES_DEBUG=AUM
E_O_F
  systemctl daemon-reload
  systemctl restart apertis-update-manager
}

branch_name()
{
	if [ "${BOARD}" = "rpi64" ]; then
		echo "apertis/${RELEASE}/${ARCH}-uboot/${IMGTYPE}"
	else
		echo "apertis/${RELEASE}/${ARCH}-${BOARD}/${IMGTYPE}"
	fi
}

# Prepare the commit which is outdated.
# We create the commit with a copy of the current boot but with the timestamp from the past.
# It allows to upgrade the system to the current version with the common upgrade method,
# this allows to test:
# - check if the system could be upgraded to the same commit as already booted
# - test what the update manager and libostree from current boot are able to upgrade
#   the OS to future releases
prepare_outdated_commit()
{
	ostree admin status

	# Create a new commit with the same content than the current deployment, but older
	BRANCHNAME=$(branch_name)
	CURREV=$(ostree rev-parse origin:$BRANCHNAME)
	ostree commit -b $BRANCHNAME --tree=ref=$CURREV --timestamp="1 year ago"
	OLDREV=$(ostree rev-parse $BRANCHNAME)
	ostree admin upgrade --os=apertis --allow-downgrade --deploy-only --override-commit=$OLDREV

	# Store the commit with prepared revision for use in tests later
	echo $OLDREV > $(get_phase_data_path)
	sync
}

EFIBOOTDIR="/boot/efi/loader/entries"

# Function validate values from /boot/uboot.cnt or
# EFI /boot/efi/loader/entries/ostree-0-2[+bootleft[-bootcount]].conf
# accept 4 arguments:
#  magic: bd
#  version: 1
#  bootcount: any number
#  upgrade bit:
#              0: not in upgrade
#              1: in upgrade
# In case if any argument is not defined i.e. passed empty value ""
# it is meaning no need to validate that argument
check_bootcounter()
{
	if [ -f /boot/uboot.cnt ]; then
		uboot_counter=$(od -t x1 -An /boot/uboot.cnt)
		m=$(echo $uboot_counter | cut -d ' ' -f 1)
		# Special case for old format
		if [ "$m" = "bc" ]; then
			cnt=$(echo $uboot_counter | cut -d ' ' -f 2)
			ver="00"
			# NB: due the bug in legacy U-Boot -- it always create the uboot.cnt and
			# increase the counter, so it is not possible to detect the upgrade
			# state properly. Hence set the upgrade state for the legacy into
			# requested value to avoid failures.
			uflag="${4:-1}"
		else
			ver=$(echo $uboot_counter | cut -d ' ' -f 2)
			cnt=$(echo $uboot_counter | cut -d ' ' -f 3)
			uflag=$(echo $uboot_counter | cut -d ' ' -f 4)
		fi
	elif [ -f ${EFIBOOTDIR}/ostree-0-2*.conf ]; then
		m="$1"
		ver="${2:-00}"
		local suffix=$(ls ${EFIBOOTDIR}/ostree-0-2*.conf | cut -d "+" -f 2 -s | cut -d "." -f 1 -s)
		if [ -z "$suffix" ]; then
			cnt="00"
			uflag="00"
		else
			uflag="01"
			cnt=$(echo $suffix | cut -d "-" -f 2 -s)
			if [ -z "$cnt" ]; then
				cnt="00"
			else
				cnt=$(printf "%02d" "$cnt")
			fi
		fi
	else
		echo "No boot counter found"
		return 1
	fi

	local ret=0
	local magic="${1:-$m}"
	local version="${2:-$ver}"
	local bootcount="${3:-$cnt}"
	local upgrade="${4:-$uflag}"

	# Validate via expected values
	test "$magic" = "$m" || ret=1
	test "$version" -eq "$ver" || ret=1
	test "$bootcount" -eq "$cnt" || ret=1
	test "$upgrade" -eq "$uflag" || ret=1

	echo "Expected bootcounter: $magic, $version, $bootcount, $upgrade"
	echo "    Read bootcounter: $m, $ver, $cnt, $uflag"
	return $ret
}

# Function to reset the boot counter
# Supporting old, new and EFI formats
# Accept 2 arguments:
#  bootcounter: number, 0 if skip
#  upgrade flag (could be omitted):
#    1: set upgrade state (default)
#    0: disable upgrade state
set_bootcounter()
{
	local bootcount="${1:-0}"
	local upgrade_flag="${2:-1}"

	if [ -d ${EFIBOOTDIR} ]; then
		bootentry=$(ls ${EFIBOOTDIR}/ostree-0-2*.conf)
		if [ -n "$bootentry" ]; then
			if [ "$upgrade_flag" -eq 1 ]; then
				if [ "$bootcount" -eq 0 ]; then
					mv $bootentry ${EFIBOOTDIR}/ostree-0-2+4.conf
				else
					bootleft=$((4 - $bootcount))
					if [ $bootleft -ge 0 ]; then
						mv $bootentry ${EFIBOOTDIR}/ostree-0-2+${bootleft}-${bootcount}.conf
					else
						mv $bootentry ${EFIBOOTDIR}/ostree-0-2+0-${bootcount}.conf
					fi
				fi
			else
				# Remove the counters in filename to mark we are not in upgrade state
				mv $bootentry ${EFIBOOTDIR}/ostree-0-2.conf
			fi
		fi
	else
		local magic="bc"
		test -f /boot/uboot.cnt && magic="$(od -An -t x1 -N1 /boot/uboot.cnt | tr -d ' ')"

		case "$magic" in
			"bc")	# Old style "BC 00"
				if [ "$upgrade_flag" -eq 1 ]; then
					echo "bc $(printf "%.2x" $bootcount)" | busybox xxd -r -p > /boot/uboot.cnt
				else
					# Remove the file to mark we are not in upgrade state
					rm -f /boot/uboot.cnt
				fi
				;;
			"bd")	# New style "BD 01 00 01"
				echo "bd 01 $(printf "%.2x" $bootcount) $(printf "%.2x" $upgrade_flag)" | busybox xxd -r -p > /boot/uboot.cnt
				;;
			*)
				echo "Unknown magic ($magic) in /boot/uboot.cnt"
				return 1
				;;
		esac
	fi
	sync
}

# Fill the disk with a big file which will later be truncated to keep so free space required
# for normal operations
# This was reworked in this way to have more control over the free space left
fill_disk()
{
	busybox dd if=/dev/zero of=/var/bigfile bs=1M || true
	local MAGIC=1
	busybox du -m /var/bigfile
	local FILE_SIZE=$(busybox du -m /var/bigfile | awk '{ print $1-'$MAGIC' }')
	busybox truncate -s ${FILE_SIZE}M /var/bigfile
}

# Undo the fill disk operation by removing the file created
undo_fill_disk()
{
	rm -f /var/bigfile
}
