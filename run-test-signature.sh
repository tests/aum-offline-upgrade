#!/bin/sh

. ./update-manager-common.sh

delta_no_sign="static-update-no_sign.bundle"
delta_sign="static-update-sign.bundle"
delta_no_superblock_sign="static-update-no_superblock_sign.bundle"
apertis_pk="/usr/share/ostree/trusted.ed25519.d/apertis.ed25519"

phase_boot()
{
    # Check file with public key availability
    testname="test-sign-public-key-avail"
    if test -f ${apertis_pk} ; then
        echo "${testname}: pass"
    else
        echo "${testname}: fail"
        exit 1
    fi

    # Check local configuration
    testname="test-sign-config"
    sign_verify=$(ostree config get 'remote "origin".sign-verify')

    if [ "${sign_verify}" != "true" ]; then
        echo "${testname}: fail"
        exit 1
    else
        echo "${testname}: pass"
    fi
    unset sign_verify

    # Save the Apertis public key for later usage
    cp -av ${apertis_pk} apertis.ed25519

    # Generate keys and save public key for later usage
    gen_ed25519_keys
    echo ${ED25519PUBLIC} > superblock.ed25519

    # Prepare the skiplist
    # Rationale: https://phabricator.apertis.org/T5881
    # In addition: remove all public keys from the future rootfs
    ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo /usr/share/ostree/trusted.ed25519.d > /tmp/skip

    ostree admin status

    # Create a new commit with the same content than the current deployment, but older
    BRANCHNAME=$(branch_name)
    CURREV=$(ostree rev-parse origin:$BRANCHNAME)
    ostree commit -b $BRANCHNAME --tree=ref=$CURREV --timestamp="1 year ago" --skip-list=/tmp/skip
    OLDREV=$(ostree rev-parse $BRANCHNAME)
    ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$OLDREV

    # Additional signed upgrade bundle
    ostree static-delta generate \
        --from=${OLDREV} \
        --to=${CURREV} \
        --inline \
        --min-fallback-size=1024 \
        --disable-bsdiff \
        --sign-type=ed25519 --sign=${ED25519SECRET} \
        --filename=${delta_sign}

    # Additional non-signed superblock upgrade bundle
    ostree static-delta generate \
        --from=${OLDREV} \
        --to=${CURREV} \
        --inline \
        --min-fallback-size=1024 \
        --disable-bsdiff \
        --filename=${delta_no_superblock_sign}

    # Additional non-signed upgrade bundle
    # Should not contain signed update
    LOCALREV=$(ostree commit --orphan --add-metadata-string=ostree.collection-binding="org.apertis.os" --bind-ref=$BRANCHNAME --tree=ref=$OLDREV)
    ostree static-delta generate \
        --from=${OLDREV} \
        --to=${LOCALREV} \
        --inline \
        --min-fallback-size=1024 \
        --disable-bsdiff \
        --filename=${delta_no_sign}

    # Store the commit with prepared revision for use in tests later
    echo $OLDREV > $(get_phase_data_path)
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_update()
{
    ostree admin status

    ## Non-signed update
    # Update must fail
    testname="test-sign-non_signed"
    ret=0
    apply_update_sync -d ${delta_no_sign} || ret=$?
    if [ $ret -eq 1 ] ; then
        echo "${testname}: pass"
    else
        echo "${testname}: fail"
        exit 1
    fi

    # Signed with a signature with unknown public key
    # Update must fail
    testname="test-sign-no_signature"
    ret=0
    apply_update_sync -d ${delta_sign} || ret=$?
    if [ $ret -eq 1 ] ; then
        echo "${testname}: pass"
    else
        echo "${testname}: fail"
        exit 1
    fi

    # Add the public key into the system
    mkdir -p "/etc/ostree/trusted.ed25519.d"
    cp -av apertis.ed25519 /etc/ostree/trusted.ed25519.d/

    # Signed commits but superblock not signed
    # Update must fail
    testname="test-sign-no_superblock_signature"
    ret=0
    apply_update_sync -d ${delta_no_superblock_sign} || ret=$?
    if [ $ret -eq 1 ] ; then
        echo "${testname}: pass"
    else
        echo "${testname}: fail"
        exit 1
    fi

    # Signed commits but superblock signed with unknown public key
    # Update must fail
    testname="test-sign-wrong_superblock_signature"
    ret=0
    apply_update_sync -d ${delta_sign} || ret=$?
    if [ $ret -eq 1 ] ; then
        echo "${testname}: pass"
    else
        echo "${testname}: fail"
        exit 1
    fi

    # Add public key for static delta superblock verification
    cp -av superblock.ed25519 /etc/ostree/trusted.ed25519.d/

    testname="test-sign-update"
    apply_update_sync -d ${delta_sign}

    ostree admin status
}

phase_check_update()
{
    ostree admin status

    local PHASE_DATA=$(get_phase_data_path)
    local COMMIT_BEFORE=$(cat $PHASE_DATA)

    # The current commit after the update
    local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

    if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
        echo "The update did not apply, the current commit is the same"
        exit 1
    fi
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_update\
	phase_check_update
