#!/bin/sh
set -ex

. ./update-manager-common.sh

phase_boot()
{
	local DELTAFILE=$(get_static_delta "_rollback" tests)

	# The currently deployed commit before the update
	local BRANCHNAME=$(branch_name)
	local CURREV=$(ostree rev-parse origin:$BRANCHNAME)

	apply_update_sync -d $DELTAFILE

	# Ensure the delta is pending
	echo $CURREV > $(get_phase_data_path)

	ostree admin status

	# Ensure to have upgrade state
	check_bootcounter "" "" 00 01
}

phase_check_update()
{
	ostree admin status

	# Enable debug for AUM to check how it deal with upgrade&rollback
	enable_aum_debug
	journalctl --no-pager -u apertis-update-manager

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same as the initial commit"
		exit 1
	fi

	# Check the bootcounter state
	# It may have different boot count on different boards due LAVA
	check_bootcounter "" "" "" 01

	# Emulate the ugrade state -- set the upgrade flag and bootcounter to any value above the limit
	# This should to trigger the U-Boot to boot previous version after reboot
	set_bootcounter 5
}

phase_check_rollback()
{
	# Wait long enough so that AUM have a chance to finish undeploy
	sleep 30

	ostree admin status
	journalctl --no-pager -u apertis-update-manager

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" != "$COMMIT_AFTER" ] ; then
		echo "The update did not rollback, the current commit is the same"
		exit 1
	fi

	# after the rollback, the apertis update manager resets the bootcount
	# Ensure to have non-upgrade state
	check_bootcounter "" "" 00 00

	cat /var/aum_blacklist.conf
}


phase_check_update_is_blacklisted()
{
	cat /var/aum_blacklist.conf
	local DELTAFILE=$(get_static_delta "_rollback" tests)

	# Ensure the update cannot apply again	
	if apply_update_sync -d $DELTAFILE ; then
		echo "Blacklisted update applied"
		exit 1
	fi

	ostree admin status
}


update_manager_phases\
	phase_boot\
	phase_check_update\
	phase_check_rollback\
	phase_check_update_is_blacklisted\
	phase_check_rollback

