#!/bin/sh

. ./update-manager-common.sh

collection_id=org.apertis.os

gen_ed25519_keys

# Add public key for static delta superblock verification
echo ${ED25519PUBLIC} > /etc/ostree/trusted.ed25519.d/superblock.ed25519

# Get booted commit ID
set -e
boot_id="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"
boot_cid=$(ostree show ${boot_id} --print-metadata-key ostree.collection-binding | tr -d "'")
# Expecting only one ref for Apertis
boot_ref=$(ostree show ${boot_id} --print-metadata-key ostree.ref-binding | tr -d "'[]")
boot_ts="$(ostree show ${boot_id} |  sed -n -e 's/^Date:  \(.*\)$/\1/p')"
boot_ts=$(date +%s --date="${boot_ts}")
set +e
echo "boot commit '${boot_id}': $boot_cid:$boot_ref $boot_ts"

# list of the collection refs which should fail
# name_suffix:collection:ref:timestamp
# String after '#' ignored
refspecs="nocidrefs::: # No collection refs
same:${boot_cid}:${boot_ref}:${boot_ts} # wrong TS -- same as booted
old:${boot_cid}:${boot_ref}:$(date +%s --date='1 year ago') # wrong TS -- in the past
nocid::${boot_ref}: # No Collection ID
noref:${boot_cid}:: # No ref-bindings
wrongcid:${boot_cid}.wrong:${boot_ref}: # Non-expected Collection ID
wrongref:${boot_cid}:${boot_ref}/wrong: # Non-expected ref-binding
"

echo "${refspecs}" | sed -e s/"#.*"// | \
    while read -r refspec; do
        [ -n "${refspec}" ] || continue
        # read have no '-d' option :(
        suffix="${refspec%%:*}"; refspec="${refspec#*:}"
        cid="${refspec%%:*}"; refspec="${refspec#*:}"
        ref="${refspec%%:*}"
        ts="${refspec#*:}"

        # Command for creation wrong commit
        cmd_ostree_commit="ostree commit --orphan --tree=ref=${boot_id}"
        [ -z "${cid}" ] || cmd_ostree_commit="${cmd_ostree_commit} --add-metadata-string=ostree.collection-binding=${cid}"
        [ -z "${ref}" ] || cmd_ostree_commit="${cmd_ostree_commit} --bind-ref=${ref}"
        [ -z "${ts}" ]  || cmd_ostree_commit="${cmd_ostree_commit} --timestamp=@${ts}"

        testname="test-wrong-collection_id-${suffix}"
        delta_id=$(${cmd_ostree_commit})
        delta_file=static-update-${suffix}.bundle

        # Create delta with wrong metadata
        ostree static-delta generate \
            --from=${boot_id} \
            --to ${delta_id} \
            --inline \
            --min-fallback-size=1024 \
            --disable-bsdiff \
            --sign-type=ed25519 --sign=${ED25519SECRET} \
            --filename=${delta_file}

        # Update must fail!
        ret=0
        apply_update_sync -d ${delta_file} || ret=$?
        if [ $ret -eq 1 ] ; then
            echo "${testname}: pass"
        else
            echo "${testname}: fail"
        fi

        # Cleanup
        ostree prune --refs-only
        rm -f ${delta_file}

    done

exit 0
