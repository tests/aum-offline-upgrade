#!/bin/sh

. ./update-manager-common.sh

save_phase_data()
{
	cat > $(get_phase_data_path) << EOF
$1
$2
EOF
	sync
}

get_phase_data()
{
	COMMIT_BEFORE=$(head -1 $(get_phase_data_path))
	DELAY=$(tail -n 1 $(get_phase_data_path))
}

phase_boot()
{
	prepare_outdated_commit

	save_phase_data "$OLDREV" "0"
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_try_update()
{
	get_phase_data
	DELAY=$(($DELAY+1))
	if [ $DELAY -gt 3 ] ; then
		DELAY=3
	fi
	save_phase_data "$COMMIT_BEFORE" "$DELAY"

	DELTAFILE=$(get_static_delta "")

	# Spawn the update asynchronously
	apply_update_async $DELTAFILE

	# Blindly wait until the update is effectively in progress
	# FIXME This test should be repeated as long as possible
	sleep $DELAY

	ostree admin status

	# Now lava will force a reboot because it believes the test ended
}

phase_check_update()
{
	get_phase_data

	ostree admin status

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \(.*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same: ok"
	else
		echo "The update applied, the current commit has changed ($COMMIT_BEFORE != $COMMIT_AFTER)"
		exit 1
	fi
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_try_update\
	phase_try_update\
	phase_try_update\
	phase_try_update\
	phase_check_update
